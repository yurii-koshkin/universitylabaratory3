package org.game.units;

import org.game.utils.GameUtils;

import java.util.List;

public class Medical extends Droid {
    private final int healPower = 150;
    private final int healChance = 50;

    public Medical(String name) {
        super.setName(name);
        super.setDamage(15);
        super.setHealth(1500);
        super.setAvoidanceChance(20);
        super.setCriticalChance(20);
    }

    @Override
    public void fight(List<Droid> teammates, List<Droid> enemies) {
        if (this.healChance >= GameUtils.getLuckyScore()) {
            GameUtils.writeEvent(super.getName() + " is insane!!!");
            this.healTeammates(teammates);
        }

        super.hit(enemies);
    }

    private void healTeammates(List<Droid> teammates) {
        for (Droid teammate : teammates) {
            teammate.setHealth(teammate.getHealth() + this.healPower);
            GameUtils.writeEvent(super.getName() + " has healed " + teammate.getName() + " by " + this.healPower + " HP!");
        }
    }

}
