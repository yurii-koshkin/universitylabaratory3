package org.game.units;

import org.game.utils.GameUtils;

import java.util.List;

public class Assassin extends Droid {
    private final int oneShotChance = 15;

    public Assassin(String name) {
        super.setName(name);
        super.setDamage(60);
        super.setHealth(1000);
        super.setAvoidanceChance(50);
        super.setCriticalChance(30);
    }

    @Override
    public void fight(List<Droid> teammates, List<Droid> enemies) {
        if (this.oneShotChance >= GameUtils.getLuckyScore()) {
            System.out.println(super.getName() + " is insane!!!");
            this.killEnemy(enemies);
        } else {
            super.hit(enemies);
        }
    }

    private void killEnemy(List<Droid> enemies) {
        Droid chosenEnemy = GameUtils.getRandomDroid(enemies);
        GameUtils.writeEvent(chosenEnemy.getName() + " is defeat (one shot) by " + this.getName() + "!");
        enemies.removeIf(droid -> droid == chosenEnemy);
    }
}
