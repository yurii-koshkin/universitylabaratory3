package org.game.units;

import org.game.utils.GameUtils;

import java.util.List;

public class Fighter extends Droid {
    private final int bulkHitChance = 40;

    public Fighter(String name) {
        super.setName(name);
        super.setDamage(200);
        super.setHealth(2500);
        super.setAvoidanceChance(5);
        super.setCriticalChance(10);
    }

    @Override
    public void fight(List<Droid> teammates, List<Droid> enemies) {
        if (this.bulkHitChance >= GameUtils.getLuckyScore()) {
            GameUtils.writeEvent(super.getName() + " is insane!!!");
            this.bulkHit(enemies);
        } else {
            super.hit(enemies);
        }
    }

    private void bulkHit(List<Droid> enemies) {
        for (int i = enemies.size() - 1; i >= 0; i--) {
            int actualDamage = super.getCriticalChance() > GameUtils.getLuckyScore() ? super.getDamage() * 2 : super.getDamage();
            enemies.get(i).setHealth(enemies.get(i).getHealth() - actualDamage);
            GameUtils.writeEvent(this.getName() + " damaged " + enemies.get(i).getName() + " by " + actualDamage + " HP!");

            if (enemies.get(i).getHealth() == 0) {
                int finalI = i;
                GameUtils.writeEvent(enemies.get(i).getName() + " is defeat by " + this.getName() + "!");
                enemies.removeIf(droid -> droid == enemies.get(finalI));
            }
        }
    }

}
