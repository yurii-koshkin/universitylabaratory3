import org.game.arenas.Arena;
import org.game.arenas.DefaultArena;
import org.game.units.Assassin;
import org.game.units.Droid;
import org.game.units.Fighter;
import org.game.units.Medical;
import org.game.utils.GameUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws InterruptedException {
        List<Droid> ourTeam = new ArrayList<>();
        List<Droid> enemiesTeam = new ArrayList<>();

        System.out.println("Enter '1' to add Droid to our team ");
        System.out.println("Enter '2' to add Droid to enemies team ");
        System.out.println("Enter '3' to remove Droid from our team ");
        System.out.println("Enter '4' to remove Droid from enemies team ");
        System.out.println("Enter '5' to start battle ");
        System.out.println("Enter '6' to repeat last battle ");
        System.out.println("Enter '7' to exit ");

        int option = 0;
        while (option != 7) {
            System.out.print("Enter option: ");
            option = scanner.nextInt();

            switch (option) {
                case (1) -> addDroidToTeam(ourTeam);
                case (2) -> addDroidToTeam(enemiesTeam);
                case (3) -> removeDroidFromTeam(ourTeam);
                case (4) -> removeDroidFromTeam(enemiesTeam);
                case (5) -> startBattle(ourTeam, enemiesTeam);
                case (6) -> GameUtils.readFromFile();
                case (7) -> System.out.println("See you later!");
                default -> System.out.println("This option does not exist");
            }
        }
    }

    private static void addDroidToTeam(List<Droid> team) {
        scanner.nextLine();
        System.out.print("Enter Droid name: ");
        String name = scanner.nextLine();

        System.out.println("Enter '1' to create Medical");
        System.out.println("Enter '2' to create Fighter");
        System.out.println("Enter '3' to create Assassin");
        System.out.print("Enter class: ");
        int option = scanner.nextInt();

        Droid droid = null;
        switch (option) {
            case (1) -> droid = new Medical(name);
            case (2) -> droid = new Fighter(name);
            case (3) -> droid = new Assassin(name);
            default -> System.out.println("This option does not exist");
        }

        if (droid != null) {
            team.add(droid);
        }
    }

    private static void removeDroidFromTeam(List<Droid> team) {
        for (int i = 1; i <= team.size(); i++) {
            System.out.println("Enter " + i + " to remove droid " + team.get(i - 1).getName());
        }

        System.out.print("Enter Droid: ");
        int option = scanner.nextInt();

        if (option > 1 || option > team.size()) {
            System.out.println("This option does not exist");
        } else {
            team.remove(option - 1);
        }
    }

    private static void startBattle(List<Droid> ourTeam, List<Droid> enemiesTeam) throws InterruptedException {
        Arena arena = new DefaultArena(ourTeam, enemiesTeam);
        arena.startBattle();
    }

}