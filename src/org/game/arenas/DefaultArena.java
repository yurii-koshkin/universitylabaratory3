package org.game.arenas;

import org.game.units.Droid;

import java.util.List;

public class DefaultArena extends Arena {


    public DefaultArena(List<Droid> teammates, List<Droid> enemies) {
        super.setName("Polytech");
        super.setDamageModification(3);
        super.setHealthModification(1);
        super.setTeammates(teammates);
        super.setEnemies(enemies);
        super.applyModification();
    }

}
