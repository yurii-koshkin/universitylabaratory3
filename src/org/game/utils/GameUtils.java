package org.game.utils;

import org.game.units.Droid;

import java.io.*;
import java.util.List;
import java.util.Random;

public class GameUtils {
    private static final Random random = new Random();
    private static final PrintWriter printWriter;

    static {
        try {
            printWriter = new PrintWriter(new BufferedWriter(new FileWriter("saved.txt", false)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static int getLuckyScore() {
        return random.nextInt(100);
    }

    public static Droid getRandomDroid(List<Droid> droids) {
        return droids.get(random.nextInt(droids.size()));
    }

    public static void writeEvent(String text) {
        printWriter.println(text);
        System.out.println(text);
    }

    public static void finishWriting() {
        printWriter.close();
    }

    public static void readFromFile() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("saved.txt"))) {
            String line = bufferedReader.readLine();

            while (line != null) {
                if (line.equals("=================================================================")) {
                    Thread.sleep(2500);
                }

                System.out.println(line);

                line = bufferedReader.readLine();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
