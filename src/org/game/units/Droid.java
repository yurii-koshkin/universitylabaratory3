package org.game.units;

import org.game.utils.GameUtils;

import java.util.List;

public abstract class Droid {
    private String name;
    private int damage;
    private int avoidanceChance;
    private int criticalChance;
    private int health = 100;

    public abstract void fight(List<Droid> teammates, List<Droid> enemies);

    protected void hit(List<Droid> enemies) {
        Droid chosenEnemy = GameUtils.getRandomDroid(enemies);
        if (chosenEnemy.getAvoidanceChance() <= GameUtils.getLuckyScore()) {
            int actualDamage = this.getDamage();
            if (this.getCriticalChance() > GameUtils.getLuckyScore()) {
                actualDamage *= 2;
                GameUtils.writeEvent(this.getName() + " is even stronger today!");
            }

            chosenEnemy.setHealth(chosenEnemy.getHealth() - actualDamage);
            GameUtils.writeEvent(this.getName() + " damaged " + chosenEnemy.getName() + " by " + actualDamage + " HP!");
        } else {
            GameUtils.writeEvent(chosenEnemy.getName() + " has dodged from " + this.getName() + " attack!");
        }

        if (chosenEnemy.getHealth() == 0) {
            enemies.removeIf(droid -> droid == chosenEnemy);
            GameUtils.writeEvent(chosenEnemy.getName() + " is defeat by " + this.getName() + "!");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return Math.max(health, 0);
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getAvoidanceChance() {
        return avoidanceChance;
    }

    public void setAvoidanceChance(int avoidanceChance) {
        this.avoidanceChance = avoidanceChance;
    }

    public int getCriticalChance() {
        return criticalChance;
    }

    public void setCriticalChance(int criticalChance) {
        this.criticalChance = criticalChance;
    }

    @Override
    public String toString() {
        return "\nName: " + name + "\nHealth: " + health + "\nDamage: " + damage;
    }
}
