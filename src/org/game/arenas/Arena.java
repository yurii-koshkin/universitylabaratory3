package org.game.arenas;

import org.game.units.Droid;
import org.game.utils.GameUtils;

import java.util.ArrayList;
import java.util.List;

public abstract class Arena {
    private String name;

    private double damageModification = 1;
    private double healthModification = 1;

    private List<Droid> teammates;
    private List<Droid> enemies;

    public void startBattle() throws InterruptedException {
        GameUtils.writeEvent("\n=================================================================\n");
        boolean isOurTurn = GameUtils.getLuckyScore() % 2 == 0;
        while (!teammates.isEmpty() && !enemies.isEmpty()) {
            if (isOurTurn) {
                GameUtils.getRandomDroid(teammates).fight(teammates, enemies);
            } else {
                GameUtils.getRandomDroid(enemies).fight(enemies, teammates);
            }

            isOurTurn = !isOurTurn;

            GameUtils.writeEvent("\n-----------------------------------------------------------------");
            this.showHealthPoints();
            Thread.sleep(2500);
            GameUtils.writeEvent("\n=================================================================\n");
        }

        if (!teammates.isEmpty()) {
            GameUtils.writeEvent("We won!");
        } else {
            GameUtils.writeEvent("We lost(");
        }

        GameUtils.finishWriting();
    }

    protected void applyModification() {
        if (this.damageModification == 1 && this.healthModification == 1) {
            return;
        }

        List<Droid> droids = new ArrayList<>();
        droids.addAll(teammates);
        droids.addAll(enemies);

        for (Droid droid : droids) {
            droid.setDamage((int) (droid.getDamage() * this.damageModification));
            droid.setHealth((int) (droid.getHealth() * this.healthModification));
        }
    }

    private void showHealthPoints() {
        GameUtils.writeEvent("\nOur team: ");
        for (Droid droid : teammates) {
            GameUtils.writeEvent(droid.getName() + " : " + droid.getHealth() + " HP");
        }

        GameUtils.writeEvent("\nEnemies team: ");
        for (Droid droid : enemies) {
            GameUtils.writeEvent(droid.getName() + " : " + droid.getHealth() + " HP");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHealthModification() {
        return healthModification;
    }

    public void setHealthModification(double healthModification) {
        this.healthModification = healthModification;
    }

    public double getDamageModification() {
        return damageModification;
    }

    public void setDamageModification(double damageModification) {
        this.damageModification = damageModification;
    }

    public List<Droid> getTeammates() {
        return teammates;
    }

    public void setTeammates(List<Droid> teammates) {
        this.teammates = teammates;
    }

    public List<Droid> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<Droid> enemies) {
        this.enemies = enemies;
    }
}
